﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

abstract public class Command : MonoBehaviour
{
    public Unit ThisUnit;
    public bool StartCommand = false;
    public bool IsCommandCompleted = false;

    public Command()
    {

    }

    protected virtual void Awake()
    {

    }

    protected virtual void Start()
    {

    }

    protected virtual void Update()
    {

    }

    public virtual void CommandSetUp()
    {

    }

    protected void CleanUpCommand()
    {
        if(IsCommandCompleted == true)
        {
            ThisUnit.CurrentCommand = null;
            Destroy(this);
        }
    }
}
