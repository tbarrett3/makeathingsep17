﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackUnitCommand : Command {

    public Tile TileToAttack;
    public int AttackDamage = 1;

    public bool IsAttacking = false;
    public float UnitTime;
    public float AttackDelay;
    public bool HasAttacked = false;

    // Use this for initialization
    protected override void Start () {
		
	}

    protected override void Update()
    {
        UnitTime += Time.deltaTime;

        if (StartCommand == false)
            return;

        if (UnitTime < AttackDelay)
            return;

        if(HasAttacked == false)
            BasicAttack();
        CheckOnBeatReset();
        CleanUpCommand();
    }

    private void PlayAnimation()
    {

    }

    private void PlaySound()
    {

    }

    public void BasicAttack()
    {
        TileToAttack.OccupyingUnit.CurrentHealth = TileToAttack.OccupyingUnit.CurrentHealth - AttackDamage;
        Debug.Log("Fight");
        HasAttacked = true;
    }
    public void CheckOnBeatReset()
    {
        if(UnitTime >= (TimeKeeper.Instance.BeatInterval - 0.1f))
        {
            IsCommandCompleted = true;
        }
    }

    public void AttackCommandSetUp(Unit _ThisUnit, EnumManager.Direction _MovementDirection, int _AttackDamage)
    {
        ThisUnit = _ThisUnit;
        AttackDamage = _AttackDamage;

        switch (_MovementDirection)
        {
            case EnumManager.Direction.North:
                TileToAttack = _ThisUnit.OccuyingTile.NorthTile;
                break;
            case EnumManager.Direction.South:
                TileToAttack = _ThisUnit.OccuyingTile.SouthTile;
                break;
            case EnumManager.Direction.East:
                TileToAttack = _ThisUnit.OccuyingTile.EastTile;
                break;
            case EnumManager.Direction.West:
                TileToAttack = _ThisUnit.OccuyingTile.WestTile;
                break;
        }
    }
}
