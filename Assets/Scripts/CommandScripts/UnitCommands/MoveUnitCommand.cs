﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveUnitCommand : Command {

    [FMODUnity.EventRef]

    public Tile TileToMoveTo;
    float UnitMovementSpeed = 5.0f;
    float MoveDelay;

    public bool IsMoving = false;
    float UnitTime;

    protected override void Awake()
    {
        MoveDelay = Random.Range(0f, 0.2f);
    }

    protected override void Update()
    {
        UnitTime += Time.deltaTime;

        if (StartCommand == false)
            return;

        if (UnitTime < MoveDelay)
            return;

        BasicMove();
        CheckMovement();
        CleanUpCommand();
    }

    private void AssignMove()
    {
        ThisUnit.OccuyingTile = TileToMoveTo;
        ThisUnit.transform.SetParent(TileToMoveTo.transform);
    }

    private void PlayAnimation()
    {
        
    }

    private void PlaySound()
    {
        FMODUnity.RuntimeManager.PlayOneShot(ThisUnit.MovementSoundFModEventRef, transform.position);
    }

    private void BasicMove()
    {
        if (ThisUnit.OccuyingTile != null && TileToMoveTo != null && IsMoving == true)
        {
            Vector3 DirectionOfTile = TileToMoveTo.UnitPoition - ThisUnit.transform.position;
            ThisUnit.transform.Translate(DirectionOfTile.normalized * UnitMovementSpeed * Time.deltaTime);
            if (Vector3.Distance(ThisUnit.transform.position, TileToMoveTo.UnitPoition) <= 0.05f)
            {
                ThisUnit.transform.position = TileToMoveTo.UnitPoition;
            }
        }
    }

    private void CheckMovement()
    {
        if(TileToMoveTo == null)
            IsCommandCompleted = true;
        else
        {
            if (ThisUnit.transform.position == TileToMoveTo.UnitPoition)
            {
                IsMoving = false;
                IsCommandCompleted = true;
            }
        }
    }

    public void MoveCommandSetUp(Unit _ThisUnit, EnumManager.Direction _MovementDirection)
    {
        ThisUnit = _ThisUnit;

        switch (_MovementDirection)
        {
            case EnumManager.Direction.North:
                TileToMoveTo = _ThisUnit.OccuyingTile.NorthTile;
                break;
            case EnumManager.Direction.South:
                TileToMoveTo = _ThisUnit.OccuyingTile.SouthTile;
                break;
            case EnumManager.Direction.East:
                TileToMoveTo = _ThisUnit.OccuyingTile.EastTile;
                break;
            case EnumManager.Direction.West:
                TileToMoveTo = _ThisUnit.OccuyingTile.WestTile;
                break;
        }

        if(TileToMoveTo != null && TileToMoveTo.OccupyingUnit == null)
        {
            ThisUnit.OccuyingTile.OccupyingUnit = null;
            ThisUnit.OccuyingTile = TileToMoveTo;
            ThisUnit.transform.SetParent(TileToMoveTo.transform);
            TileToMoveTo.OccupyingUnit = ThisUnit;
            IsMoving = true;
        }
    }
}
