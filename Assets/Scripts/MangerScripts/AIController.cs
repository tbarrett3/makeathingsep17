﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIController : MonoBehaviour {

    public static AIController Instance;

    public List<EnemyUnit> CurrentEnemies = new List<EnemyUnit>();


    private void Awake()
    {
        if (Instance == null)
            Instance = this;
        else
            Destroy(this);
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(TimeKeeper.Instance.IsOnBeat == true)
        {
            CheckUnitsForActions();
        }
        if (TimeKeeper.Instance.IsOnBeat == false)
        {
            CheckForCompleatedActions();
        }
    }

    void CheckUnitsForActions()
    {
        foreach (EnemyUnit Enemy in CurrentEnemies)
        {
            int SpawnCheck = TimeKeeper.Instance.ActionsPast % Enemy.MovementInterval;

            if (SpawnCheck == 0 && Enemy.CanTakeAction == true && Enemy.OccuyingTile.WestTile != null)
            {
                if (Enemy.OccuyingTile.WestTile.OccupyingUnit == null)
                {
                    Enemy.CanTakeAction = false;
                    CommandManager.Instance.NewMoveUnitCommand(Enemy, EnumManager.Direction.West);
                }
            }
        }
    }

    void CheckForCompleatedActions()
    {
        foreach (EnemyUnit Enemy in CurrentEnemies)
        {
            if(Enemy.CurrentCommand == null)
            {
                Enemy.CanTakeAction = true;
            }
        }
    }
}
