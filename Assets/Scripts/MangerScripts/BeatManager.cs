﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeatManager : MonoBehaviour {

    public AudioSource BeatSource;
    public AudioClip BeatClip;

    public bool HasTrackStarted = false;
	// Use this for initialization
	void Start () {
        if(BeatClip != null)
        {
            BeatSource.clip = BeatClip;
        }
    }
	
	// Update is called once per frame
	void Update () {
		if(HasTrackStarted == false)
        {
            if(TimeKeeper.Instance.CurrentTime >= TimeKeeper.Instance.BeatInterval)
            {
                BeatSource.PlayOneShot(BeatClip);
                HasTrackStarted = true;
            }
        }
	}
}
