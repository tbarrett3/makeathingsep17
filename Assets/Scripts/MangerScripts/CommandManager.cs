﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class CommandManager : MonoBehaviour {

    public static CommandManager Instance;

    public List<Command> MovementComands = new List<Command>();
    public List<Command> CombatCommands = new List<Command>();

    void Awake()
    {
        if (Instance == null)
            Instance = this;
        else
            Destroy(this);
    }

    void Update()
    {
        ActOnMovementCommands();
        ActOnCombatCommands();

        CleanUpMovementList();
        CleanUpCombatList();
    }

    void ActOnMovementCommands()
    {
        foreach (Command MoveCommand in MovementComands)
            MoveCommand.StartCommand = true;

    }

    void CleanUpMovementList()
    {
        bool IsACommandNotCompleted = false;
        foreach (Command MoveCommand in MovementComands)
        {
            if(MoveCommand.IsCommandCompleted == false)
            {
                IsACommandNotCompleted = true;
                return;
            }
        }
        if(IsACommandNotCompleted == false)
            MovementComands.Clear();
    }



    void ActOnCombatCommands()
    {
        foreach (Command CombatCommand in CombatCommands)
            CombatCommand.StartCommand = true;

    }

    void CleanUpCombatList()
    {
        bool IsACommandNotCompleted = false;
        foreach (Command CombatCommand in CombatCommands)
        {
            if (CombatCommand.IsCommandCompleted == false)
            {
                IsACommandNotCompleted = true;
                return;
            }
        }
        if (IsACommandNotCompleted == false)
            CombatCommands.Clear();
    }




    public void NewMoveUnitCommand(Unit _UnitToMove, EnumManager.Direction _MoveDirection)
    {

        MoveUnitCommand NewMoveCommand = _UnitToMove.gameObject.AddComponent<MoveUnitCommand>();
        _UnitToMove.CurrentCommand = NewMoveCommand;
        NewMoveCommand.MoveCommandSetUp(_UnitToMove, _MoveDirection);
        MovementComands.Add(NewMoveCommand);
    }


    public void NewAttackUnitCommand(Unit AttackingUnit, EnumManager.Direction _MoveDirection, int _AttackDamage)
    {

        AttackUnitCommand NewAttackCommand = AttackingUnit.gameObject.AddComponent<AttackUnitCommand>();
        AttackingUnit.CurrentCommand = NewAttackCommand;
        NewAttackCommand.AttackCommandSetUp(AttackingUnit, _MoveDirection, _AttackDamage);
        CombatCommands.Add(NewAttackCommand);
    }
}
