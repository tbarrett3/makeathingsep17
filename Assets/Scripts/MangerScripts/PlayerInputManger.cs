﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInputManger : MonoBehaviour {

    public static PlayerInputManger Instance;

    public Unit PlayerUnit;

    public bool CanTakeInput = true;

    public string MovePlayerNorthKey;
    public string MovePlayerSouthKey;
    public string MovePlayerEastKey;
    public string MovePlayerWestKey;

    public int XAxisGridClamp = 3;

    private void Awake()
    {
        if (Instance == null)
            Instance = this;
        else
            Destroy(this);
    }

    void Update()
    {
        if (PlayerUnit.CurrentCommand == null)
            CanTakeInput = true;
        if (CanTakeInput == true)
        {
            ReciveAndActOnInput();
        }
    }


    void ReciveAndActOnInput()
    {
        if (CanTakeInput == false || TimeKeeper.Instance.IsOnBeat == false)
            return;

        if (Input.GetKeyDown(MovePlayerNorthKey) && PlayerUnit != null && PlayerUnit.OccuyingTile.NorthTile != null)
        {
            CanTakeInput = false;
            if(PlayerUnit.OccuyingTile.NorthTile.OccupyingUnit != null)
            {
                CommandManager.Instance.NewAttackUnitCommand(PlayerUnit, EnumManager.Direction.North, 1);
            }
            else 
            {
                CommandManager.Instance.NewMoveUnitCommand(PlayerUnit, EnumManager.Direction.North);
            }

        }
        if (Input.GetKeyDown(MovePlayerSouthKey) && PlayerUnit != null && PlayerUnit.OccuyingTile.SouthTile != null)
        {
            CanTakeInput = false;
            if (PlayerUnit.OccuyingTile.SouthTile.OccupyingUnit != null && PlayerUnit.OccuyingTile.SouthTile != null)
            {
                CommandManager.Instance.NewAttackUnitCommand(PlayerUnit, EnumManager.Direction.South, 1);
            }
            else 
            {
                CommandManager.Instance.NewMoveUnitCommand(PlayerUnit, EnumManager.Direction.South);
            }
        }
        if (Input.GetKeyDown(MovePlayerEastKey) && PlayerUnit != null && PlayerUnit.OccuyingTile.EastTile != null 
            && PlayerUnit.OccuyingTile.GridPosition.x != XAxisGridClamp - 1)
        {
            CanTakeInput = false;
            if (PlayerUnit.OccuyingTile.EastTile.OccupyingUnit != null && PlayerUnit.OccuyingTile.EastTile != null)
            {
                CommandManager.Instance.NewAttackUnitCommand(PlayerUnit, EnumManager.Direction.East, 1);
            }
            else 
            {
                CommandManager.Instance.NewMoveUnitCommand(PlayerUnit, EnumManager.Direction.East);
            }
        }
        if (Input.GetKeyDown(MovePlayerWestKey) && PlayerUnit != null && PlayerUnit.OccuyingTile.WestTile != null)
        {
            CanTakeInput = false;
            if (PlayerUnit.OccuyingTile.WestTile.OccupyingUnit != null)
            {
                CommandManager.Instance.NewAttackUnitCommand(PlayerUnit, EnumManager.Direction.West, 1);
            }
            else 
            {
                CommandManager.Instance.NewMoveUnitCommand(PlayerUnit, EnumManager.Direction.West);
            }
        }
    }
}
