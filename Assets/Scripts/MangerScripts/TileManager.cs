﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileManager : MonoBehaviour {

    public static TileManager Instance;
    public int GridZHeight = 4;
    public int GridXLength = 7;
    public float TileSpacing;

    public int MinSpawnInterval = 5;
    public int MaxSpawnInterval = 15;

    public GameObject TilePrefab;
    public GameObject PlayerPrefab;

    public Unit PlayerUnit;
    public GameObject BasicEnemyPrefab;

    public List<EnemySpawner> Spawners = new List<EnemySpawner>();

    //[ZHeight, XLength]
    public Tile[,] TileMatrix;

    public Material SpawnTileMat;

    private void Awake()
    {
        if (Instance == null)
            Instance = this;
        else
            Destroy(this);
    }

    // Use this for initialization
    void Start () {
        CreateGrid();
        SpawnPlayer();
        SetTileNeighbours();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void CreateGrid()
    {
        TileMatrix = new Tile[GridZHeight, GridXLength + 1];

        if (TilePrefab == null)
            return;

        for (int XIndex = 0; XIndex < GridXLength + 1; ++XIndex)
        {
            for (int ZIndex = 0; ZIndex < GridZHeight; ++ZIndex)
            {
                GameObject NewTile = Instantiate(TilePrefab);
                Tile TileScript = NewTile.GetComponent<Tile>();
                TileMatrix[ZIndex, XIndex] = TileScript;
                NewTile.transform.position = new Vector3(XIndex, 0, ZIndex);
                NewTile.transform.SetParent(this.transform);
                TileScript.GridPosition = new Vector2(XIndex, ZIndex);
                TileScript.InitialSetup();

                if (XIndex == GridXLength)
                    SetSpawnTile(NewTile);
            }
        }
    }

    void SetSpawnTile(GameObject _NewTile)
    {
        _NewTile.name = "SpawnTile";
        _NewTile.GetComponent<Renderer>().material = SpawnTileMat;
        _NewTile.AddComponent<EnemySpawner>().InitalSetUp(5, Random.Range(MinSpawnInterval, MaxSpawnInterval), 100, BasicEnemyPrefab);
    }

    void SetTileNeighbours()
    {
        foreach(Tile Tile in TileMatrix)
        {
            if(Tile != null)
                Tile.NeighbouringTileSetup();
        }
    }

    void SpawnPlayer()
    {
        if (PlayerPrefab == null)
            return;

        GameObject NewPlayer = Instantiate(PlayerPrefab);
        NewPlayer.transform.SetParent(TileMatrix[0, 0].transform);
        PlayerUnit = NewPlayer.GetComponent<PlayerCharacterUnit>();
        TileMatrix[0,0].OccupyingUnit = PlayerUnit;
        PlayerUnit.transform.position = TileMatrix[0,0].UnitPoition;
        PlayerInputManger.Instance.PlayerUnit = PlayerUnit;
        PlayerUnit.OccuyingTile = TileMatrix[0, 0];
        
    }
}
