﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeKeeper : MonoBehaviour {

    public static TimeKeeper Instance;

    public float SongBPM = 120;
    public float BeatInterval;
    public float CurrentTime;
    public float NextBpmAction;
    public float PreviousBpmAction;
    public bool IsOnBeat = false;

    public int ActionsPast = 0;

    public float BeatTolerance = 0.1f;

    private void Awake()
    {
        if (Instance == null)
            Instance = this;
        else
            Destroy(this);
    }

    // Use this for initialization
    void Start () {
        BeatInterval = (1 / (SongBPM / 60));

    }
	
	// Update is called once per frame
	void Update () {
        CurrentTime += Time.deltaTime;
        UpdateActionTime();
        CheckForWithinBeat();
    }

    void UpdateActionTime()
    {
        if(NextBpmAction < CurrentTime)
        {
            PreviousBpmAction = NextBpmAction;
            NextBpmAction += BeatInterval;
            ++ActionsPast;
        }
    }

    void CheckForWithinBeat()
    {
        if( (CurrentTime > NextBpmAction - BeatTolerance) || (CurrentTime < PreviousBpmAction + BeatTolerance))
        {
            IsOnBeat = true;
            return;
        }
            IsOnBeat = false;
    }
}
