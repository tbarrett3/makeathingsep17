﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour {

    public GameObject BasicEnemyPrefab;
    public Tile TileToSpawnOn;

    public int FirstSpawnBeat;
    public int SpawnBeatInterval;
    public int EnemiesToSpawn;

    public int SpawnCheck;

    public List<GameObject> EnimiesToSpawn = new List<GameObject>();


	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
        SpawnEnemy();

    }

    void SpawnEnemy()
    {
        SpawnCheck = TimeKeeper.Instance.ActionsPast % SpawnBeatInterval;

        if (SpawnCheck == 0 && TileToSpawnOn.OccupyingUnit == null)
        {
            if(EnemiesToSpawn != 0)
            {
                GameObject SpawnedEnemy = Instantiate(BasicEnemyPrefab);
                EnemyUnit ThisEnemy = SpawnedEnemy.GetComponent<EnemyUnit>();
                ThisEnemy.SpawnSetup(TileToSpawnOn);
                AIController.Instance.CurrentEnemies.Add(ThisEnemy);
                --EnemiesToSpawn;
            }
        }
    }

    public void InitalSetUp(int _FirstSpawnBeat, int _SpawnBeatInterval, int _EnemiesToSpawn, GameObject _BasicEnemyPrefab)
    {
        TileToSpawnOn = gameObject.GetComponent<Tile>();
        FirstSpawnBeat = _FirstSpawnBeat;
        SpawnBeatInterval = _SpawnBeatInterval;
        EnemiesToSpawn = _EnemiesToSpawn;
        BasicEnemyPrefab = _BasicEnemyPrefab;
    }
}
