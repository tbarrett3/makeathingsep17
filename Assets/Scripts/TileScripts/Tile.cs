﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile : MonoBehaviour {

    public Vector2 GridPosition;

    private static float OccupyingUnitHeightPos = 0.5f;
    public Vector3 UnitPoition;
    public Unit OccupyingUnit = null;
    public Tile NorthTile = null;
    public Tile SouthTile = null;
    public Tile EastTile = null;
    public Tile WestTile = null;

    // Use this for initialization
    void Start () {
        InitialSetup();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void InitialSetup()
    {
        UnitPoition = new Vector3(transform.position.x, transform.position.y + OccupyingUnitHeightPos, transform.position.z);
    }


    public void NeighbouringTileSetup()
    {
        if(GridPosition.x != TileManager.Instance.GridXLength)
            EastTile = TileManager.Instance.TileMatrix[(int)GridPosition.y, (int)GridPosition.x + 1];
        if (GridPosition.x != 0)
            WestTile = TileManager.Instance.TileMatrix[(int)GridPosition.y, (int)GridPosition.x - 1];

        if (GridPosition.y != TileManager.Instance.GridZHeight - 1)
            NorthTile = TileManager.Instance.TileMatrix[(int)GridPosition.y + 1, (int)GridPosition.x];
        if (GridPosition.y != 0)
            SouthTile = TileManager.Instance.TileMatrix[(int)GridPosition.y - 1, (int)GridPosition.x];
    }
}
