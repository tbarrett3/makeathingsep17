﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyUnit : Unit {

    public int MovementInterval;
    public bool CanTakeAction = true;

    // Use this for initialization
    void Start()
    {
        UnitInitalSetup();
    }

    // Update is called once per frame
    void Update()
    {
        FlashColour();
        CheckForDeath();
    }

    public void SpawnSetup(Tile _OccupyingTile)
    {
        OccuyingTile = _OccupyingTile;
        _OccupyingTile.OccupyingUnit = this;
        gameObject.transform.SetParent(OccuyingTile.transform);
        transform.position = OccuyingTile.UnitPoition;
    }
}
