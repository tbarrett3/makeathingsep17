﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCharacterUnit : Unit {

    public static PlayerCharacterUnit Instance;

    private void Awake()
    {
        if (Instance == null)
            Instance = this;
        else
            Destroy(this);
    }

    // Use this for initialization
    void Start () {
        UnitInitalSetup();
    }
	
	// Update is called once per frame
	void Update () {
        FlashColour();
	}
}
