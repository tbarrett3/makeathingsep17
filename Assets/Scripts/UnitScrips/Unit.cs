﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

abstract public class Unit : MonoBehaviour {

    public int UnitHealth;
    public int CurrentHealth;
    public Tile OccuyingTile;

    public Command CurrentCommand = null;

    public Animation MovementAnimation;
    public string MovementSoundFModEventRef;
    public Animation AttackAnimation;
    public string AttackSoundFModEventRef;

    public Material BeatMaterial;
    public Material DefaultMaterial;
    public Renderer ThisRenderer;

	// Use this for initialization
	void Start () {

    }
	
	// Update is called once per frame
	void Update () {


    }

    protected void InitiateMovement(Tile _MovementTile)
    {

    }

    protected void FlashColour()
    {
        if(TimeKeeper.Instance.IsOnBeat == true)
        {
            ThisRenderer.material = BeatMaterial;
        }
        else
        {
            ThisRenderer.material = DefaultMaterial;
        }
    }

    protected void UnitInitalSetup()
    {
        ThisRenderer = GetComponent<Renderer>();
        DefaultMaterial = ThisRenderer.material;
        CurrentHealth = UnitHealth;
    }

    protected void CheckForDeath()
    {
        if(CurrentHealth == 0)
        {
            AIController.Instance.CurrentEnemies.Remove(GetComponent<EnemyUnit>());
            Destroy(gameObject);
        }
    }
}
